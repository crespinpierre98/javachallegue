package com.api.javaChallegue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaChallegueApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaChallegueApplication.class, args);
	}

}
